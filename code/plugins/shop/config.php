<?php
if (!defined('IN_FINECMS')) exit('No permission resources');

return array(
	'key' => 12,
    'name'    => '在线购物',
    'author'  => 'dayrui',
    'version' => '1.0',
    'typeid'  => 1,
    'description' => "支持购物车，在线订单，余额支付等购物商城",
    'fields' => array(
       
    )
);